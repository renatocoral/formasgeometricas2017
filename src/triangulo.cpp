#include "triangulo.hpp"

Triangulo::Triangulo(){
   setBase(0);
   setAltura(0);
   tipo = "Triângulo";
}

Triangulo::Triangulo(float base, float altura){
  setBase(base);
  setAltura(altura);
  tipo = "Triângulo";
}

float Triangulo::area(){
   return getBase()*getAltura()/2;
}
