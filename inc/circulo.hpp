#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica {
  private:
  	float raio;
  public:
   Circulo();
   Circulo(float raio);
   float getRaio();
   void setRaio(float raio);

   float area();
};

#endif
