#include "formageometrica.hpp"

FormaGeometrica::FormaGeometrica(){
   setBase(0.0);
   setAltura(0.0);
   tipo = "Genérica";
}

FormaGeometrica::FormaGeometrica(float base, float altura){
   setBase(base);
   setAltura(altura);
   tipo = "Genérica";
}

float FormaGeometrica::getBase(){
   return base;
}
void FormaGeometrica::setBase(float base){
   this->base = base;
}
float FormaGeometrica::getAltura(){
   return altura;
}
void FormaGeometrica::setAltura(float altura){
   this->altura = altura;
}
string FormaGeometrica::getTipo(){
   return tipo;
}

float FormaGeometrica::area(){
   return base*altura;
}


