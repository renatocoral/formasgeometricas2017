#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "formageometrica.hpp"

class Triangulo : public FormaGeometrica {
  public:
   Triangulo();
   Triangulo(float base, float altura);

   float area();
};

#endif
