#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include <iostream>
using namespace std;

int main (int argc, char ** argv){

 // FormaGeometrica * forma_1 = new FormaGeometrica();
 // FormaGeometrica * forma_2 = new FormaGeometrica(10.0, 20.0);

  Triangulo * triangulo_1 = new Triangulo();
  Triangulo * triangulo_2 = new Triangulo(15.0, 30.0);
/* 
  cout << "Forma: " << forma_1->getTipo() << " Área = " << forma_1->area() << endl; 
  cout << "Forma: " << forma_2->getTipo() << " Área = " << forma_2->area() << endl;
  cout << "Forma: " << triangulo_1->getTipo() << " Área = " << triangulo_1->area() << endl;
  cout << "Forma: " << triangulo_2->getTipo() << " Área = " << triangulo_2->area() << endl;
*/

   FormaGeometrica *lista_de_formas[10];

   lista_de_formas[0] = new Triangulo(5.0, 10.0);
   lista_de_formas[1] = new Triangulo(8.0, 10.0);
   lista_de_formas[2] = new Circulo(3.0);
   lista_de_formas[3] = new Circulo(5.0);

   for (int i = 0; i < 4; i++) {
      cout << "Forma: " << lista_de_formas[i]->getTipo() << " Área = " << lista_de_formas[i]->area() << endl;

   }   



  return 0;





}
