#include "circulo.hpp"

#define PI 3.1415926536f

Circulo::Circulo(){
   setBase(0);
   setAltura(0);
   tipo = "Círculo";
}

Circulo::Circulo(float raio){
  setRaio(raio);
  tipo = "Círculo";
}
float Circulo::getRaio(){
	return raio;
}
void Circulo::setRaio(float raio){
	this->raio = raio;
}

float Circulo::area(){
   return PI*getRaio()*getRaio();
}
