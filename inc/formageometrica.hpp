#ifndef FORMAGEOMETRICA_HPP
#define FORMAGEOMETRICA_HPP

#include <string>

using namespace std;

class FormaGeometrica {

   private:
      float base, altura;
   protected:    
      string tipo;
   public:
      FormaGeometrica();
      FormaGeometrica(float base, float altura);

      float getBase();
      void setBase(float base);
      float getAltura();
      void setAltura(float altura);
      string getTipo();

      virtual float area() = 0;      

};
#endif
